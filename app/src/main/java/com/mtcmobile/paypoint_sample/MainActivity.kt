package com.mtcmobile.paypoint_sample

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        findViewById<Button>(R.id.button).setOnClickListener {
            receiptTest()
            simpleTestB()
        }
    }

    private fun receiptTest() {
        var testReceipt = ""
        val PaypointLogo = "424dd6010000000000003e000000280000003d00000033000000010001000000000098010000232e0000232e0000000000000000000000000000ffffff00ffff1ffffffffff8ffff0ffffffffff8ffffc7fffffffff81f89c71fc388e7081f01c71f8088e6081f31831f1c88e6381f19931f1c08e638018191011c08e63800f111001c884638180139188088041818863818c3888c1800ffff00fffffe3801ffff01ff8ffe38fffffffffffffff8fffffffffffffff8fffffffffffffff80003ff8003fffff80003ff8003fffff80003ff8003fffff80003ff8003fffff80003ff8003fffff80003ff8003fffff80003ff8003fffff80003ff8003fffff80003ff8003fffff80003ff8003fffff800000078000007f80000007e000001f80000007f000000780000007f800000380000007fc00000180000007fe00000180000007fe00000080000007fe00000080007f07ff3f800000007f87ff3fc00000007fc7ff3fe00000007fc7ff3fe00000007fe7ff3ff00000007fc7ff3fe00000007fc7ff3fe00000007f87ff3fc00000007e07ff3f000000000007fe00000080000007fe00000080000007fc00000180000007fc00000380000007f800000380000007e000000f80000007c000003f80000006000001ff8"
        val receipt = JSONObject()
        val receiptLines = JSONArray()
        val logoLine = JSONObject()
        val headerLine = JSONObject()
        try {
            headerLine.put("text", "LogoCreator Output")
            logoLine.put("lineType", 2)
            logoLine.put("text", PaypointLogo)
            receiptLines.put(headerLine)
            receiptLines.put(logoLine)
            receipt.put("receiptType", 5)
            receipt.put("receiptlines", receiptLines)
            testReceipt = receipt.toString()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        simpleTestA(testReceipt)
    }

    private fun simpleTestA(json: String) {
        try {
            val intent = Intent("com.canfordsoftware.PRINTER")
            intent.putExtra("json", json)
            intent.putExtra("priority", 0) // LOW PRIORITY
            intent.putExtra("packagename", packageName)
            sendBroadcast(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun simpleTestB() {
        try {
            val text = "This is test print text"
            val raw = text.toByteArray()


            val intent = Intent("com.canfordsoftware.PRINTER")
            intent.putExtra("raw", raw);
            intent.putExtra("priority", 0) // LOW PRIORITY
            intent.putExtra("packagename", packageName)
            sendBroadcast(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}